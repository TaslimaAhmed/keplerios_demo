//
//  OnBoardingViewController.swift
//  Kepler
//
//  Created by Mufad Monwar on 6/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class OnBoardingViewController: UIViewController {

    
    
    
    @IBOutlet weak var btn_sign_in: UIButton!
    @IBOutlet weak var btn_language: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
    }
    
    
    private func setupView(){

        btn_sign_in.layer.borderWidth = 1.0
        btn_sign_in.layer.borderColor = UIColor.red.cgColor

        btn_language.layer.borderWidth = 1.0
        btn_language.layer.borderColor = UIColor.red.cgColor
    }


}
