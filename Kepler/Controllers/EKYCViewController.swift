//
//  EKYCViewController.swift
//  Kepler
//
//  Created by Mufad Monwar on 4/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class EKYCViewController: UIViewController {
    
    @IBOutlet weak var btn_next_ekyc: UIButton!
    @IBOutlet weak var tf_user_name: UITextField!
    @IBOutlet weak var tf_user_dob: UITextField!
    @IBOutlet weak var tf_user_nid: UITextField!
    @IBOutlet weak var tf_user_gender: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
    }
    
    private func setupViews(){
        
        //btn_next_ekyc.layer.cornerRadius = 28.0
        
        
        tf_user_name.layer.borderWidth = 1.0
        tf_user_name.layer.cornerRadius = 2.0
        tf_user_name.layer.borderColor = UIColor.red.cgColor
        
        
        tf_user_dob.layer.borderWidth = 1.0
        tf_user_dob.layer.cornerRadius = 2.0
        tf_user_dob.layer.borderColor = UIColor.red.cgColor
        
        
        tf_user_nid.layer.borderWidth = 1.0
        tf_user_nid.layer.cornerRadius = 2.0
        tf_user_nid.layer.borderColor = UIColor.red.cgColor
        
        
        tf_user_gender.layer.borderWidth = 1.0
        tf_user_gender.layer.cornerRadius = 2.0
        tf_user_gender.layer.borderColor = UIColor.red.cgColor
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
