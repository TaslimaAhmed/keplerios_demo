//
//  SetupPinViewController.swift
//  Kepler
//
//  Created by Mufad Monwar on 4/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class SetupPinViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var btn_confirm: UIButton!
    @IBOutlet weak var tf_pin_input: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
    }
    
    private func setupViews(){
       // btn_confirm.layer.cornerRadius = 28.0
        tf_pin_input.layer.borderWidth = 1.0
        tf_pin_input.layer.cornerRadius = 2.0
        tf_pin_input.layer.borderColor = UIColor.red.cgColor
        tf_pin_input.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tf_pin_input.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        let currentString: NSString  = tf_pin_input.text! as NSString
        
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
