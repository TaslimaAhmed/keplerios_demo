//
//  VerifyOTPViewController.swift
//  Kepler
//
//  Created by Mufad Monwar on 6/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class VerifyOTPViewController: UIViewController , UITextFieldDelegate{
    
    @IBOutlet weak var btn_next: UIButton!
    @IBOutlet weak var tf_otp: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        // Do any additional setup after loading the view.
        
        
    }
    
    private func setupViews(){
        tf_otp.layer.borderWidth = 1.0
        tf_otp.layer.cornerRadius = 2.0
        tf_otp.layer.borderColor = UIColor.red.cgColor
        tf_otp.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tf_otp.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        let currentString: NSString  = tf_otp.text! as NSString
        
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
}
