//
//  MobileNumberViewController.swift
//  Kepler
//
//  Created by Mufad Monwar on 6/6/20.
//  Copyright © 2020 BDKepler. All rights reserved.
//

import UIKit

class MobileNumberViewController: UIViewController , UITextFieldDelegate{
    
    
    @IBOutlet weak var container_number: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextField()
        
    }
    
    private func setupTextField(){
        // Do any additional setup after loading the view.
        container_number.layer.borderWidth = 1.0
        container_number.layer.borderColor = UIColor.red.cgColor
        container_number.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        container_number.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 8
        let currentString: NSString  = container_number.text! as NSString
        
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
